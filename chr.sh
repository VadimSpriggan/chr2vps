#!/bin/bash

CHR_VERSION=6.49.15 
INPATH=/dev/`lsblk -dn | cut -c-3`
ROOTPASS=Gksai3280HUG73912! #write your password for ROOT user

ADDRESS=`ip addr show | grep global | cut -d ' ' -f 6 | head -n 1` && \
GATEWAY=`ip route list | grep default | cut -d ' ' -f 3` && \

apt update && apt install -y wget coreutils && \
wget https://download.mikrotik.com/routeros/${CHR_VERSION}/chr-${CHR_VERSION}.img.zip -O chr.img.zip && \
gunzip -c chr.img.zip > chr.img && \
mount -o loop,offset=512 chr.img /mnt && \
echo "/ip address add address=$ADDRESS network=$GATEWAY broadcast=$ADDRESS interface=[/interface ethernet find where name=ether1]
/ip route add gateway=$GATEWAY
/ip dns set servers=1.1.1.1,8.8.8.8
/ip service disable telnet
/user set 0 name=root password=$ROOTPASS"
echo u > /proc/sysrq-trigger && \
dd if=chr.img bs=1024 of=$INPATH && \
echo "sync disk" && \
echo s > /proc/sysrq-trigger && \
echo "Sleep 5 seconds" && \
sleep 30 && \
echo "Ok, reboot" && \
echo b > /proc/sysrq-trigger